# DueDateCalculator

* [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) Composer
* Run `php composer.phar install`
* Run unit tests 
	* `vendor/bin/phpunit tests/` or 
	* `vendor/bin/phpunit tests/ --configuration tests/phpunit.xml  --coverage-html tests/coverage`
* There is a test script available called `duedate.php` where you can set the submitted date and turnaround time
	* Example usage: `php duedate.php -s '2017-05-22 9:00' -t 6`
 