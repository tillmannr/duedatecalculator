<?php

namespace DueDate\Validator;

use DueDate\Workday\Workday;

class Validator
{
	/**
	 * @var Workday
	 */
	private $workday;

	public function __construct(Workday $workday)
	{
		$this->workday = $workday;
	}

	public static function create()
	{
		return new self(Workday::create());
	}

	public function validate(\DateTime $date, int $turnaroundTime)
	{
		$this->validateWorkHours($date);
		$this->validateTurnaroundTime($turnaroundTime);
	}

	private function validateWorkHours(\DateTime $date)
	{
		if (!$this->workday->isWithinWorkHours($date) || !$this->workday->isWorkday($date)) {
			throw new ValidatorException('Submit date is not within work hours.');
		}
	}

	private function validateTurnaroundTime($turnaroundTime)
	{
		if ($turnaroundTime < 0) {
			throw new ValidatorException('Turnaround time should be greater than 0.');
		}
	}
}