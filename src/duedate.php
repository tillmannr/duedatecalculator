<?php

chdir(__DIR__);

include('../vendor/autoload.php');

$help = <<<HELP

Usage:

duedate.php -s SUBMITTED_DATE -t TURNAROUND_HOURS
eg. duedate.php -s '2017-05-26 09:00' -t 9


HELP;

$opts = getopt('s:t:');
if (!isset($opts['s'], $opts['t'])) {
	print $help;
	exit;
}

try {
	$submitDate        = new DateTime($opts['s']);
	$turnaroundTime    = $opts['t'];
	$dueDateCalculator = DueDate\Calculator::create();

	print $dueDateCalculator->calculateDueDate($submitDate, $turnaroundTime)->format('Y-m-d H:i:s l');
} catch (Exception $e) {
	print $e->getMessage();
}

