<?php

namespace DueDate;

use DueDate\Validator\Validator;
use DueDate\Workday\Workday;

class Calculator
{
	/**
	 * @var Workday
	 */
	private $workday;

	/**
	 * @var Validator
	 */
	private $validator;

	public function __construct(Workday $workday, Validator $validator)
	{
		$this->workday   = $workday;
		$this->validator = $validator;
	}

	public static function create()
	{
		return new self(Workday::create(), Validator::create());
	}

	public function calculateDueDate(\DateTime $submitDate, int $turnaroundTime): \DateTime
	{
		$this->validator->validate($submitDate, $turnaroundTime);

		$turnaroundDays  = floor($turnaroundTime / $this->workday->getWorkHoursPerDay());
		$turnaroundHours = $turnaroundTime % $this->workday->getWorkHoursPerDay();

		$this->workday->addWorkdays($submitDate, $turnaroundDays);
		$this->workday->addWorkHours($submitDate, $turnaroundHours);

		return $submitDate;
	}
}