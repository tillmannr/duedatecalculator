<?php

namespace DueDate\Workday;

class Workday
{
	const WORKDAY_START = '09:00';
	const WORKDAY_END   = '17:00';

	public static function create()
	{
		return new self();
	}

	public function isWithinWorkHours(\DateTime $date): bool
	{
		$time = \DateTime::createFromFormat('H:i', $date->format('H:i'));
		return $time >= $this->getWorkdayStart() && $time <= $this->getWorkdayEnd();
	}

	public function isWorkday(\DateTime $date): bool
	{
		return $date->format('N') < 6;
	}

	public function addWorkdays(\DateTime $date, int $days): \DateTime
	{
		while ($days > 0) {
			$this->addWorkday($date);
			$days--;
		}

		return $date;
	}

	public function addWorkday(\DateTime $date): \DateTime
	{
		do {
			$date->modify('+1 day');
		} while (!$this->isWorkday($date));

		return $date;
	}

	public function addWorkHours(\DateTime $date, int $hours): \DateTime
	{
		$date->modify(sprintf("+ %d hours", $hours));
		if (!$this->isWithinWorkHours($date)) {
			$this->roundToNextWorkday($date);
		}

		return $date;
	}

	public function roundToNextWorkday(\DateTime $date): \DateTime
	{
		$this->addWorkday($date);
		return $date->modify(
			sprintf("- %d hours", $this->getWorkHoursPerDay())
		);
	}

	public function getWorkHoursPerDay(): int
	{
		return (int)$this->getWorkdayEnd()->diff($this->getWorkdayStart())->format('%h');
	}

	private function getWorkdayStart(): \DateTime
	{
		return \DateTime::createFromFormat('H:i', self::WORKDAY_START);
	}

	private function getWorkdayEnd(): \DateTime
	{
		return \DateTime::createFromFormat('H:i', self::WORKDAY_END);
	}
}