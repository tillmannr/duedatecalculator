<?php

use DueDate\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorIntegrationTest extends TestCase
{
	/**
	 * @var Calculator
	 */
	private $calculator;

	public function setUp()
	{
		$this->calculator = Calculator::create();
	}

	/**
	 * @dataProvider sameDayDataProvider
	 */
	public function testDueDateIsOnTheSameDay($submitDate, $turnaround, $dueDate)
	{
		$this->assertEquals($dueDate, $this->calculator->calculateDueDate($submitDate, $turnaround));
	}

	public function sameDayDataProvider()
	{
		return [
			[new DateTime('2017-05-25 09:00'), 0, new DateTime('2017-05-25 09:00')],
			[new DateTime('2017-05-25 09:00'), 3, new DateTime('2017-05-25 12:00')],
			[new DateTime('2017-05-25 09:00'), 7, new DateTime('2017-05-25 16:00')],
			[new DateTime('2017-05-25 12:00'), 4, new DateTime('2017-05-25 16:00')],
			[new DateTime('2017-05-25 15:55'), 1, new DateTime('2017-05-25 16:55')],
		];
	}

	/**
	 * @dataProvider nextDayWithoutWeekendsDataProvider
	 */
	public function testDueDateIsOnTheNextDayWithoutWeekends($submitDate, $turnaround, $dueDate)
	{
		$this->assertEquals($dueDate, $this->calculator->calculateDueDate($submitDate, $turnaround));
	}

	public function nextDayWithoutWeekendsDataProvider()
	{
		return [
			// thursday, friday
			[new DateTime('2017-05-25 09:00'), 8,  new DateTime('2017-05-26 9:00')],
			[new DateTime('2017-05-25 09:30'), 9,  new DateTime('2017-05-26 10:30')],
			[new DateTime('2017-05-25 09:00'), 12, new DateTime('2017-05-26 13:00')],
			[new DateTime('2017-05-25 16:55'), 1,  new DateTime('2017-05-26 9:55')],
		];
	}

	/**
	 * @dataProvider nextDayWithWeekendsDataProvider
	 */
	public function testDueDateIsOnTheNextDayWithWeekends($submitDate, $turnaround, $dueDate)
	{
		$this->assertEquals($dueDate, $this->calculator->calculateDueDate($submitDate, $turnaround));
	}

	public function nextDayWithWeekendsDataProvider()
	{
		return [
			// thursday, wednesday
			[new DateTime('2017-05-25 09:00'), 8 * 4,     new DateTime('2017-05-31 9:00')],
			// thursday, monday
			[new DateTime('2017-05-25 09:00'), 8 * 2,     new DateTime('2017-05-29 9:00')],
			// thursday, tuesday
			[new DateTime('2017-05-25 16:00'), 8 * 2 + 4, new DateTime('2017-05-30 12:00')],
			[new DateTime('2017-05-25 16:00'), 8 + 4,     new DateTime('2017-05-29 12:00')],
			// thursday, thursday
			[new DateTime('2017-05-25 16:00'), 8 * 10,    new DateTime('2017-06-08 16:00')],
		];
	}

	/**
	 * @dataProvider exceptionDataProvider
	 */
	public function testSubmitDateIsNotInWorkHours($submitDate)
	{
		$this->expectException(\DueDate\Validator\ValidatorException::class);
		$this->calculator->calculateDueDate($submitDate, 1);
	}

	public function exceptionDataProvider()
	{
		return [
			//saturday
			[new DateTime('2017-05-27 09:00')],
			//sunday
			[new DateTime('2017-05-28 09:00')],
			//thursday
			[new DateTime('2017-05-25 08:00')],
			[new DateTime('2017-05-25 08:59')],
			[new DateTime('2017-05-25 17:01')],
			[new DateTime('2017-05-25 00:00')],
		];
	}
}