<?php

use DueDate\Workday\Workday;
use PHPUnit\Framework\TestCase;

class WorkdayTest extends TestCase
{

	/**
	 * @var Workday
	 */
	private $workday;

	public function setUp()
	{
		$this->workday = Workday::create();
	}

	/**
	 * @dataProvider weekDayDataProvider
	 */
	public function testIsWorkDay($date, $expected)
	{
		$this->assertEquals($this->workday->isWorkday($date), $expected);
	}

	public function weekDayDataProvider()
	{
		return [
			[new DateTime('2017-05-22'), true],
			[new DateTime('2017-05-23'), true],
			[new DateTime('2017-05-24'), true],
			[new DateTime('2017-05-25'), true],
			[new DateTime('2017-05-26'), true],
			[new DateTime('2017-05-27'), false],
			[new DateTime('2017-05-28'), false],
		];
	}

	/**
	 * @dataProvider workHoursDataProvider
	 */
	public function testIsWithinWorkHours($date, $expected)
	{
		$this->assertEquals($this->workday->isWithinWorkHours($date), $expected);
	}

	public function workHoursDataProvider()
	{
		return [
			[new DateTime('2017-05-22 09:00'), true],
			[new DateTime('2017-05-22 12:00'), true],
			[new DateTime('2017-05-22 17:00'), true],
			[new DateTime('2017-05-22 18:00'), false],
			[new DateTime('2017-05-22 08:00'), false],
		];
	}

	/**
	 * @dataProvider addWorkDaysDataProvider
	 */
	public function testAddWorkDays($startDate, $days, $expected)
	{
		$this->assertEquals($this->workday->addWorkdays($startDate, $days), $expected);
	}

	public function addWorkDaysDataProvider()
	{
		return [
			[new DateTime('2017-05-22'), 0, new DateTime('2017-05-22')],
			[new DateTime('2017-05-22'), 1, new DateTime('2017-05-23')],
			[new DateTime('2017-05-26'), 1, new DateTime('2017-05-29')],
		];
	}

	/**
	 * @dataProvider addWorkHoursDataProvider
	 */
	public function testAddWorkHours($startDate, $hours, $expected)
	{
		$this->assertEquals($this->workday->addWorkHours($startDate, $hours), $expected);
	}

	public function addWorkHoursDataProvider()
	{
		return [
			[new DateTime('2017-05-22 09:00'), 0, new DateTime('2017-05-22 09:00')],
			[new DateTime('2017-05-22 09:00'), 1, new DateTime('2017-05-22 10:00')],
			[new DateTime('2017-05-22 09:00'), 7, new DateTime('2017-05-22 16:00')],
		];
	}

	/**
	 * @dataProvider roundToNextWorkDayDataProvider
	 */
	public function testRoundToNextWorkDay($date, $expected)
	{
		$this->assertEquals($this->workday->roundToNextWorkday($date), $expected);
	}

	public function roundToNextWorkDayDataProvider()
	{
		return [
			[new DateTime('2017-05-22 17:02'), new DateTime('2017-05-23 09:02')],
			[new DateTime('2017-05-22 19:02'), new DateTime('2017-05-23 11:02')],
			[new DateTime('2017-05-22 22:02'), new DateTime('2017-05-23 14:02')],
			[new DateTime('2017-05-22 00:02'), new DateTime('2017-05-22 16:02')],
		];
	}
}