<?php

use DueDate\Validator\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{

	/**
	 * @var Validator
	 */
	private $validator;


	/**
	 * @var PHPUnit_Framework_MockObject_MockObject
	 */
	private $workday;


	public function setUp()
	{
		$this->workday    = $this->getMockBuilder('\DueDate\Workday\Workday')->disableOriginalConstructor()->getMock();
		$this->validator = new Validator($this->workday);
	}

	public function testValidSubmittedDateAndTurnaround()
	{
		$submittedDate = new DateTime('2017-05-22 09:00');

		$this->workday->expects($this->once())->method('isWithinWorkHours')->willReturn(true);
		$this->workday->expects($this->once())->method('isWorkday')->willReturn(true);
		$this->validator->validate($submittedDate, 1);
	}

	public function testInvalidWorkHour()
	{
		$this->expectException(\DueDate\Validator\ValidatorException::class);
		$submittedDate = new DateTime('2017-05-22 08:00');

		$this->workday->expects($this->once())->method('isWithinWorkHours')->willReturn(false);
		$this->workday->expects($this->any())->method('isWorkday')->willReturn(true);
		$this->validator->validate($submittedDate, 1);
	}

	public function testInvalidWorkDay()
	{
		$this->expectException(\DueDate\Validator\ValidatorException::class);
		$submittedDate = new DateTime('2017-05-28 09:00');

		$this->workday->expects($this->once())->method('isWithinWorkHours')->willReturn(true);
		$this->workday->expects($this->once())->method('isWorkday')->willReturn(false);
		$this->validator->validate($submittedDate, 1);
	}

	public function testInvalidTurnaroundHour()
	{
		$this->expectException(\DueDate\Validator\ValidatorException::class);
		$submittedDate = new DateTime('2017-05-22 09:00');

		$this->workday->expects($this->once())->method('isWithinWorkHours')->willReturn(true);
		$this->workday->expects($this->once())->method('isWorkday')->willReturn(true);

		$this->validator->validate($submittedDate, -1);
	}
}